package org.open.task.master;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.time.Duration;

public class TestMaster {

	public static WebDriver driver;
	
	public static void driverInitialize() throws IOException {
		WebDriverManager.chromedriver().setup();	
		driver = new ChromeDriver();

		System.out.println("Chrome driver is initialized");
		driver.manage().window().maximize();
		System.out.println("Browser window is maximized");

		// Setting default web driver timeout to 30 SECONDS
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://www.evernote.com/Login.action");	
	}


	public static void closeDriverInstance() {
		driver.quit();
	}
}
