package org.open.task.pageobjects;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.open.task.master.TestMaster;
import org.open.task.wait.Wait;
import org.open.task.webelements.WebElementsEvernoteApp;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class EverNoteApp {

	public void loggedOut(WebDriver driver) throws InterruptedException {
		WebElementsEvernoteApp ele = new WebElementsEvernoteApp(driver);

		Wait.explicit_wait_visibilityof_webelement_120(ele.nav_user, driver);
		ele.nav_user.click();
		System.out.println("Nav User Link Clicked");

		Wait.explicit_wait_visibilityof_webelement_120(ele.logged_out_link, driver);
		ele.logged_out_link.click();
		System.out.println("Logged Out Link Clicked");
		Wait.wait5Second();
		
		try {
			ele.cancel_button.click();
			System.out.println("Cancel Logout");
			Wait.wait5Second();	
			loggedOut(driver);
		}catch (Exception e) {
			System.out.println("No Popup");
		}
	}

	public void navigateToNotes(WebDriver driver) {
		WebElementsEvernoteApp ele = new WebElementsEvernoteApp(driver);

		Wait.explicit_wait_visibilityof_webelement_120(ele.navbar_notes, driver);
		ele.navbar_notes.click();
		System.out.println("Navbar Notes Link Clicked");
		
	}
	
	public void addNewNotesButtonClick(WebDriver driver) {
		WebElementsEvernoteApp ele = new WebElementsEvernoteApp(driver);

//		Actions action = new Actions(driver);
//		action.moveToElement(ele.navbar_notes).perform();
		
		Wait.explicit_wait_visibilityof_webelement_120(ele.navbar_note_add_button, driver);
		ele.navbar_note_add_button.click();
		System.out.println("Navbar Notes Add Button Clicked");
	}
	
	public void createNotes(WebDriver driver,String title,String content) throws InterruptedException {
		WebElementsEvernoteApp ele = new WebElementsEvernoteApp(driver);
		driver.switchTo().frame(ele.notes_editor_iframe);
		Wait.explicit_wait_visibilityof_webelement_120(ele.notes_title, driver);
		ele.notes_title.sendKeys(title);
//		Wait.wait5Second();
		ele.notes_content.click();
		ele.notes_content.sendKeys(content);
		
		driver.switchTo().defaultContent();
		Wait.wait5Second();
	}
	
	public void verifyCreatedNotes(WebDriver driver,String title,String content) throws InterruptedException {
		WebElementsEvernoteApp ele = new WebElementsEvernoteApp(driver);
		String path = "//span[text()='"+title+"']";
		int i = 0;
		while(driver.findElements(By.xpath(path)).size()==0 && i!=5) {
			driver.findElement(By.id("qa-NOTES_SIDEBAR")).sendKeys(Keys.PAGE_DOWN);
			System.out.println("Scrolling");
			i++;
		}driver.findElement(By.xpath(path)).click();
		Wait.wait5Second();
		driver.switchTo().frame(ele.notes_editor_iframe);Wait.wait5Second();
//		Wait.explicit_wait_visibilityof_webelement_120(ele.notes_title_text, driver);
		String foundTitle = ele.notes_title.getAttribute("value");
		assertTrue(ele.notes_title.isDisplayed(), "Failed to verify Title Displayed");
		assertEquals(foundTitle, title, "Failed to verify Title");
		String foundContent = ele.notes_content_text.getText();
		assertTrue(ele.notes_content.isDisplayed(), "Failed to verify content Displayed");
		assertEquals(foundContent, content, "Failed to verify content");
		System.out.println("Successfully Verified Entered Notes ");
		driver.switchTo().defaultContent();
	}
	
	public void deleteSelectedNotes(WebDriver driver) {
		WebElementsEvernoteApp ele = new WebElementsEvernoteApp(driver);
		
		Wait.explicit_wait_visibilityof_webelement_120(ele.more_actions, driver);
		ele.more_actions.click();
		System.out.println("More Action Clicked");
		
		Wait.explicit_wait_visibilityof_webelement_120(ele.delete_actions, driver);
		ele.delete_actions.click();
		System.out.println("Delete Button Clicked");
	}
}
