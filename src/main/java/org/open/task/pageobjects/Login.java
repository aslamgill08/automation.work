package org.open.task.pageobjects;

import org.open.task.wait.Wait;
import org.open.task.webelements.WebElementsEvernoteApp;
import org.open.task.webelements.WebElementsLoginPage;
import org.openqa.selenium.WebDriver;
import static org.testng.Assert.*;

public class Login {

	public void clickLoginLink(WebDriver driver) {
		WebElementsLoginPage ele = new WebElementsLoginPage(driver);
		Wait.explicit_wait_visibilityof_webelement_120(ele.login_link, driver);
		ele.login_link.click();
		System.out.println("Login Link Clicked ");
	}
	
	public void enterUserName(WebDriver driver, String username) {
		WebElementsLoginPage ele = new WebElementsLoginPage(driver);
		Wait.explicit_wait_visibilityof_webelement_120(ele.userName, driver);
		ele.userName.sendKeys(username);
		System.out.println("UserName Entered : " + username);
	}

	public void enterPassword(WebDriver driver, String password) {
		WebElementsLoginPage ele = new WebElementsLoginPage(driver);
		Wait.explicit_wait_visibilityof_webelement_120(ele.password, driver);
		ele.password.sendKeys(password);
		System.out.println("Password Entered : " + password);
	}
	
	public void loginButtonClick(WebDriver driver) {
		WebElementsLoginPage ele = new WebElementsLoginPage(driver);
		Wait.explicit_wait_visibilityof_webelement_120(ele.loginButton, driver);
		ele.loginButton.click();
		System.out.println("Login Button Clicked ");
	}

	public void verifyIsLoggedIn(WebDriver driver, String isLoggedIn) {
		WebElementsLoginPage ele = new WebElementsLoginPage(driver);
		WebElementsEvernoteApp appEle = new WebElementsEvernoteApp(driver);
		if(isLoggedIn.equalsIgnoreCase("exist")) {
			Wait.explicit_wait_visibilityof_webelement_120(appEle.nav_user, driver);
			assertTrue(appEle.nav_user.isDisplayed(), "Failed to verify Logged In User");
			System.out.println("Successfully Verified User Logged In");
			
		}else if(isLoggedIn.equalsIgnoreCase("not exist")) { 

			Wait.explicit_wait_visibilityof_webelement_120(ele.responseMessage, driver);
			String foundResponseMessage = ele.responseMessage.getText();
			assertTrue(ele.responseMessage.isDisplayed(), "Failed to verify Response Message");
			assertEquals(foundResponseMessage, "There is no account for the username or email you entered.", "Failed to verify Response message");
			System.out.println("Successfully Verified There is no account for the username or email you entered. Message Displayed");
		}		
		
	}
}
