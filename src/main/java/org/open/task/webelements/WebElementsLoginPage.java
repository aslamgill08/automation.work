package org.open.task.webelements;

import org.open.task.pages.EvernoteLoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElementsLoginPage {

	WebDriver driver;

	public WebElementsLoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@FindBy(id = EvernoteLoginPage.USERNAME)
	public WebElement userName;

	@FindBy(id = EvernoteLoginPage.PASSWORD)
	public WebElement password;

	@FindBy(id = EvernoteLoginPage.LOGIN_BUTTON)
	public WebElement loginButton;

	@FindBy(id = EvernoteLoginPage.RESPONSE_MESSAGE)
	public WebElement responseMessage;

	@FindBy(xpath = EvernoteLoginPage.LOGIN_LINK)
	public WebElement login_link;
}
