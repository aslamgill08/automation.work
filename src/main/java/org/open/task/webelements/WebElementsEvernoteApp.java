package org.open.task.webelements;

import org.open.task.pages.EvernoteApp;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebElementsEvernoteApp {
	WebDriver driver;

	public WebElementsEvernoteApp(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@FindBy(id = EvernoteApp.NAV_USER)
	public WebElement nav_user;

	@FindBy(id = EvernoteApp.LOGGED_OUT_LINK)
	public WebElement logged_out_link;

	@FindBy(id = EvernoteApp.NAVBAR_NOTES)
	public WebElement navbar_notes;

	@FindBy(id = EvernoteApp.NAVBAR_NOTE_ADD_BUTTON)
	public WebElement navbar_note_add_button;
	
	@FindBy(id = EvernoteApp.NOTES_EDITOR_IFRAME)
	public WebElement notes_editor_iframe;
	
	@FindBy(xpath = EvernoteApp.NOTES_TITLE)
	public WebElement notes_title;
	
	@FindBy(xpath = EvernoteApp.NOTES_TITLE_TEXT)
	public WebElement notes_title_text;

	@FindBy(id = EvernoteApp.NOTES_CONTENT)
	public WebElement notes_content;
	
	@FindBy(xpath = EvernoteApp.NOTES_CONTENT_TEXT)
	public WebElement notes_content_text;
	
	@FindBy(id = EvernoteApp.CANCEL_BUTTON)
	public WebElement cancel_button;
	
	@FindBy(id = EvernoteApp.MORE_ACTIONS)
	public WebElement more_actions;
	
	@FindBy(id = EvernoteApp.DELETE_ACTIONS)
	public WebElement delete_actions;
}
