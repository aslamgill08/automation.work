package org.open.task.pages;

public class EvernoteApp {

	public static final String NAV_USER = "qa-NAV_USER";
	public static final String LOGGED_OUT_LINK = "qa-ACCOUNT_DROPDOWN_LOGOUT";
	
	public static final String NAVBAR_NOTES = "qa-NAV_ALL_NOTES";
	public static final String NAVBAR_NOTE_ADD_BUTTON = "qa-NAVBAR_NOTE_ADD_BUTTON";
	
	
	public static final String NOTES_EDITOR_IFRAME = "qa-COMMON_EDITOR_IFRAME";
	public static final String NOTES_TITLE = "//textarea[@placeholder='Title']";
	public static final String NOTES_TITLE_TEXT = "//textarea[@placeholder='Title']//following-sibling::div";
	public static final String NOTES_CONTENT = "en-note";
	public static final String NOTES_CONTENT_TEXT = "//*[@id='en-note']/div";
	
	
	public static final String CANCEL_BUTTON = "qa-LOGOUT_CONFIRM_DIALOG_CANCEL";
	
	public static final String MORE_ACTIONS = "qa-NOTE_ACTIONS";
	public static final String DELETE_ACTIONS = "qa-ACTION_DELETE";
}
