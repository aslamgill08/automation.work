package org.open.task.pages;

public class EvernoteLoginPage {
	
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String LOGIN_BUTTON = "loginButton";
	
	public static final String RESPONSE_MESSAGE = "responseMessage";
	
	public static final String LOGIN_LINK = "(//a[text()='Log In'])[1]";
}
