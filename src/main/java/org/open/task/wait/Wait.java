package org.open.task.wait;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class Wait {
	public static void wait1Second() throws InterruptedException {
		Thread.sleep(1000);
	}

	public static void wait2Second() throws InterruptedException {
		Thread.sleep(2000);
	}

	public static void wait3Second() throws InterruptedException {
		Thread.sleep(3000);
	}

	public static void wait5Second() throws InterruptedException {
		Thread.sleep(5000);
	}

	public static void wait10Second() throws InterruptedException {
		Thread.sleep(10000);
	}

	public static void wait15Second() throws InterruptedException {
		Thread.sleep(16000);
	}

	public static void wait25Second() throws InterruptedException {
		Thread.sleep(26000);
	}

	public static void wait40Second() throws InterruptedException {
		Thread.sleep(41000);
	}

	public static void wait30Second() throws InterruptedException {
		Thread.sleep(31000);
	}

	public static void wait60Second() throws InterruptedException {
		Thread.sleep(60000);
	}

	public static void explicit_wait_elementToBeClickable(WebElement Element, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Element));

	}

	public static void explicit_wait_visibilityof_webelement_120(WebElement Element, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(Element));

	}

	public static void WaitForElement(final WebDriver driver, final String locator) {
		FluentWait<RemoteWebDriver> wait = new FluentWait<RemoteWebDriver>((RemoteWebDriver) driver);

		// new
		// FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(50)).pollingEvery(Duration.ofSeconds(10)).ignoring(ElementNotVisibleException.class);

		wait.withTimeout(Duration.ofSeconds(200));
		wait.pollingEvery(Duration.ofSeconds(3));

		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(WebDriverException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.ignoring(ElementNotVisibleException.class);

		List<WebElement> targetElements = wait.until(new Function<RemoteWebDriver, List<WebElement>>() {
			int count = driver.findElements(By.xpath(locator)).size();

			public List<WebElement> apply(RemoteWebDriver driver) {

				List<WebElement> elements = driver.findElements(By.xpath(locator));
				int length = elements.size();

				if (length >= 1 || count > 0) {

					try {
						Thread.sleep(750);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return elements;

				}
				return null;
			}

		});
	}

	public static void waitForElementToBeVisibile(By xpath, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(xpath));

	}

	public static void waitForElementToBeClickable(By xpath, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(xpath));
	}
}
