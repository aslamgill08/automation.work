package StepDefination;

import org.open.task.master.TestMaster;
import org.open.task.pageobjects.EverNoteApp;
import org.open.task.pageobjects.Login;
import org.open.task.wait.Wait;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TC_CreateNotesAndVerify {

	Login login = new Login();
	EverNoteApp app = new EverNoteApp();
	
	@Given("Open Browser and launch the application")					
    public void open_Browser_and_launch_the_application() throws Throwable {
		TestMaster.driverInitialize();
    }
	
	@And("Login To Appliction {string}{string}")
	public void applicationLogin(String userName, String password) {
		login.enterUserName(TestMaster.driver, userName);
		login.loginButtonClick(TestMaster.driver);
		login.enterPassword(TestMaster.driver, password);
		login.loginButtonClick(TestMaster.driver);
	}
	
	@When("Create New Notes {string}{string}")
	public void createNewNotes(String title, String content) throws InterruptedException {
		app.navigateToNotes(TestMaster.driver);
		app.addNewNotesButtonClick(TestMaster.driver);
		app.createNotes(TestMaster.driver, title, content);
	}
	
	@And("Logout and Login Again {string}{string}")
	public void applicationLogout(String userName, String password) throws InterruptedException {
		
		app.loggedOut(TestMaster.driver);
//		TestMaster.driver.get("https://www.evernote.com/Login.action");
		login.clickLoginLink(TestMaster.driver);
		login.enterUserName(TestMaster.driver, userName);
		login.loginButtonClick(TestMaster.driver);
		login.enterPassword(TestMaster.driver, password);
		login.loginButtonClick(TestMaster.driver);
	}
	
	@Then("Verify Created Notes {string}{string}")
	public void verifyCreatedNotes(String title, String content) throws InterruptedException {
		app.navigateToNotes(TestMaster.driver);
		app.verifyCreatedNotes(TestMaster.driver, title, content);
		app.deleteSelectedNotes(TestMaster.driver);
		Wait.wait3Second();
	}
	
	@And("close browser instance")
	public void closeBrowser() {
		TestMaster.closeDriverInstance();
	}
}
