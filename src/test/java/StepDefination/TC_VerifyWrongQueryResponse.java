package StepDefination;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.AssertJUnit;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TC_VerifyWrongQueryResponse {

	public Response response;
	public RequestSpecification request;
	public ResponseBody jsonResponse;
	List<Object> list;
	@Given("API request wrong query {string}")
	public void apiRequestPage(String query) {
		response = RestAssured.get("https://www.metaweather.com/api/location/search/?query=" + query);
	}
	
	@When("successful response {string}")
	public void apiRequestSuccessfulResponse(String statusCode) {

		AssertJUnit.assertEquals(200, response.statusCode());
		System.out.println("Successfully Verified Response Status Code : " + response.statusCode());
		jsonResponse = response.body();
	}
	
	@Then("API request return no result")
	public void verifyResult() {
//System.out.println(response.getBody().prettyPrint());
jsonResponse = response.getBody();
String arr = jsonResponse.asPrettyString();
//System.out.println(arr);
		JSONObject root = new JSONObject("{\"data\":"+arr+"}");
		JSONArray jsonArray = root.getJSONArray("data");
//
		list = jsonArray.toList();
//System.out.println(list);
		assertEquals(list.size() + "", "0", "Failed to Verify API Request Returns Search size");
		System.out.println("Successfully Verified Total Number of records : " + list.size());


	}
	
}
