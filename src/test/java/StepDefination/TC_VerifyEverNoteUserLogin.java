package StepDefination;

import org.testng.AssertJUnit;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.open.task.master.TestMaster;
import org.open.task.pageobjects.Login;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
public class TC_VerifyEverNoteUserLogin {

	Login login = new Login();
	
	@Given("Open the Browser and launch the application")					
    public void open_the_Browser_and_launch_the_application() throws Throwable							
    {
		TestMaster.driverInitialize();
		
    }
	
	@When("Enter {string} Submit Button Clicked")
	public void enter_username_submit_button_clicked(String userName) {
		login.enterUserName(TestMaster.driver, userName);
		login.loginButtonClick(TestMaster.driver);
	}
	
	@Then("Verify User {string} and Enter {string}")
	public void verify_user_existance_and_enter_password(String exist, String password) {
		
		if(exist.equalsIgnoreCase("exist")) {
			login.enterPassword(TestMaster.driver, password);
			login.loginButtonClick(TestMaster.driver);
			login.verifyIsLoggedIn(TestMaster.driver, exist);
		}else if(exist.equalsIgnoreCase("not exist")) { 
			login.verifyIsLoggedIn(TestMaster.driver, exist);
		}
	}
	
	@And("close browser")
	public void closeBrowser() {
		TestMaster.closeDriverInstance();
	}
}
