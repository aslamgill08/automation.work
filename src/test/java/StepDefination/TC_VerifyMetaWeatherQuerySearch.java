package StepDefination;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.AssertJUnit;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TC_VerifyMetaWeatherQuerySearch {

	public Response response;
	public RequestSpecification request;
	public ResponseBody jsonResponse;
	List<Object> list;
	@Given("API request query {string}")
	public void apiRequestPage(String query) {
		response = RestAssured.get("https://www.metaweather.com/api/location/search/?query=" + query);
	}
	
	@When("Weather API request return a successful response {string}")
	public void apiRequestSuccessfulResponse(String statusCode) {

		AssertJUnit.assertEquals(200, response.statusCode());
		System.out.println("Successfully Verified Response Status Code : " + response.statusCode());
		jsonResponse = response.body();
	}
	
	@Then("API request return result contains {string}{string}")
	public void verifyResult(String query,String size) {
//System.out.println(response.getBody().prettyPrint());
jsonResponse = response.getBody();
String arr = jsonResponse.asPrettyString();
//System.out.println(arr);
		JSONObject root = new JSONObject("{\"data\":"+arr+"}");
		JSONArray jsonArray = root.getJSONArray("data");
//
		list = jsonArray.toList();
//System.out.println(list);
		assertEquals(list.size() + "", size, "Failed to Verify API Request Returns Search size");
		System.out.println("Successfully Verified Total Number of records : " + list.size());


	}
	
	@And("Weather API query request returns the following information {string}{string}{string}{string}")
	public void verifyInformation(String title, String location_type, String latt_long, String woeid) {
		for (Object obj : list) {

			@SuppressWarnings("unchecked")
			HashMap<String, Object> jsonObj = (HashMap<String, Object>) obj;
			if (jsonObj.get("title").equals(title)) {
				String acctualTitle = (String) jsonObj.get("title");
				String acctualLocationType = (String) jsonObj.get("location_type");
				String acctualLattLong = (String) jsonObj.get("latt_long");
				String acctualWoeid = (String) (jsonObj.get("woeid")+"");
				assertEquals(acctualTitle, title, "Title Failed to Verify ");
				System.out.println("Successfully Verified Title : " + acctualTitle);
				assertEquals(acctualLocationType, location_type, "LocationType Failed to Verify ");
				System.out.println("Successfully Verified LocationType : " + acctualLocationType);
				assertEquals(acctualLattLong, latt_long, "LattLong Failed to Verify ");
				System.out.println("Successfully Verified LattLong : " + acctualLattLong);
				assertEquals(acctualWoeid, woeid, "Avatar Failed to Verify ");
				System.out.println("Successfully Verified Woeid : " + acctualWoeid);

			}

		}
	}
}
