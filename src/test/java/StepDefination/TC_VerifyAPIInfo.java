package StepDefination;

import org.testng.AssertJUnit;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TC_VerifyAPIInfo {

	public Response response;
	public RequestSpecification request;
	public ResponseBody jsonResponse;

	@Given("API request page {string}")
	public void apiRequestPage(String pageNumber) {
//		RestAssured.baseURI = "https://reqres.in/api/users?page=" + pageNumber;
//		request = RestAssured.given().get("https://reqres.in/api/users?page=" + pageNumber);
		response = RestAssured.get("https://reqres.in/api/users?page=" + pageNumber);
//		response = null;
//		response = request.get();
		// 3. The page number returned matches to the one specified in the URL
		AssertJUnit.assertEquals(pageNumber, "" + response.jsonPath().getInt("page"));
		System.out.println("Successfully Verified Page Number : " + response.jsonPath().getInt("page"));

	}

	@When("API request return a successful response {string}")
	public void apiRequestSuccessfulResponse(String statusCode) {
		
		// 1. This API request return a successful response (perform any validations you
		// deem necessary)
		AssertJUnit.assertEquals(200, response.statusCode());
		System.out.println("Successfully Verified Response Status Code : " + response.statusCode());
		jsonResponse = response.body();
	}

	@Then("API request return {string} users in total")
	public void verifyTotalNumberOfUsers(String totalUsers) {
		// 2. This API request return 6 users in total3
		JSONObject root = new JSONObject(response.asString());
		JSONArray jsonArray = root.getJSONArray("data");

		List<Object> list = jsonArray.toList();

		assertEquals(list.size() + "", totalUsers, "Failed to Verify API Request Returns 6 users in total");
		System.out.println("Successfully Verified Total Number of Users : " + list.size());
		AssertJUnit.assertEquals(totalUsers, "" + response.jsonPath().getInt("per_page"));
		System.out.println("Successfully Verified Per Page Count : " + response.jsonPath().getInt("per_page"));

	}
	
	@And("API request returns the user with the following information {string}{string}{string}{string}")
	public void verifyUserInformation(String firstName, String lastName, String email, String avatar) {

		// 4. Assert that the API request returns the user with the following
		JSONObject root = new JSONObject(response.asString());
		JSONArray jsonArray = root.getJSONArray("data");

		List<Object> list = jsonArray.toList();

		for (Object obj : list) {

			@SuppressWarnings("unchecked")
			HashMap<String, Object> jsonObj = (HashMap<String, Object>) obj;
			if (jsonObj.get("first_name").equals(firstName)) {
				String acctualFirstName = (String) jsonObj.get("first_name");
				String acctualLastName = (String) jsonObj.get("last_name");
				String acctualEmail = (String) jsonObj.get("email");
				String acctualAvatar = (String) jsonObj.get("avatar");
				assertEquals(acctualFirstName, firstName, "First Name Failed to Verify ");
				System.out.println("Successfully Verified First Name : " + acctualFirstName);
				assertEquals(acctualLastName, lastName, "Last Name Failed to Verify ");
				System.out.println("Successfully Verified Last Name : " + acctualLastName);
				assertEquals(acctualEmail, email, "Email Failed to Verify ");
				System.out.println("Successfully Verified email : " + acctualEmail);
				assertEquals(acctualAvatar, avatar, "Avatar Failed to Verify ");
				System.out.println("Successfully Verified Avatar : " + acctualAvatar);

			}

		}

	}

}
