package StepDefination;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.AssertJUnit;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TC_VerifyAPIHavingNonExistentUser {

	
	public Response response;
	public RequestSpecification request;
	public ResponseBody jsonResponse;

	@Given("check it {string}")
	public void apiRequestPage(String pageNumber) {
		System.out.println(pageNumber);
		response = RestAssured.get("https://reqres.in/api/users?page="+pageNumber);
		AssertJUnit.assertEquals(pageNumber,"" +response.jsonPath().getInt("page"));
		System.out.println("Successfully Verified Page Number : " + response.jsonPath().getInt("page"));
	}
	
	@When("API request status {string}")
	public void apiRequestSuccessfulResponse(String statusCode) {
		
		// 1. This API request return a successful response (perform any validations you
		// deem necessary)
		AssertJUnit.assertEquals(200, response.statusCode());
		System.out.println("Successfully Verified Response Status Code : " + response.statusCode());
		jsonResponse = response.body();
	}
	
	@Then("API having a non-existent {string}")
	public void verifyTotalNumberOfUsers(String totalUsers) {
		// 2. This API request return 6 users in total3
		JSONObject root = new JSONObject(response.asString());
		JSONArray jsonArray = root.getJSONArray("data");

		List<Object> list = jsonArray.toList();

		assertEquals(list.size() + "", totalUsers, "Failed to Verify API Request Returns 0 users in total");
		System.out.println("Successfully Verified Total Number of Users : " + list.size());

	}
}
