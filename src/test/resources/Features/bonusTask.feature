Feature: Feature to test Weather API

  Scenario: Check API is running and giving desired result
    Given API request query "<query>"
    When Weather API request return a successful response "<status_code>"
    Then API request return result contains "<query>""<size>"
    And Weather API query request returns the following information "<title>""<location_type>""<latt_long>""<woeid>"
    
    
    Examples:
   |query			|status_code		|title		|location_type	|latt_long						|woeid|size	|
   |london		|200						|London		|City						|51.506321,-0.12714		|44418|1		|
   
  Scenario: Check API Result for Wrong query
    Given API request wrong query "<query>"
    When successful response "<status_code>"
    Then API request return no result
    
    
  Examples:
   |query			|status_code		|
   |12345			|200						|
   