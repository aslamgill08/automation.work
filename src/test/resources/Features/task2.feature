Feature: Verify EverNote features

  Scenario: Verify EverNote login
    Given Open the Browser and launch the application
    When Enter "<username>" Submit Button Clicked
    Then Verify User "<exist>" and Enter "<password>"
    And close browser

 Examples: 
     |username									|password	|exist	|
      |aslamgill08@gmal.com	 		|@slam0143|not exist 	|
	    |aslamgill08@gmail.com		|@slam0143|exist 	|
      
      
  Scenario: Verify EverNote Creation
    Given Open Browser and launch the application
    And Login To Appliction "<username>""<password>"
    When Create New Notes "<title>""<content>"
    And Logout and Login Again "<username>""<password>"
    Then Verify Created Notes "<title>""<content>"
    And close browser instance
      
  Examples: 
      |username									|password	|title			|content								|
	    |aslamgill08@gmail.com		|@slam0143|exist 			|this is first content	|
  