Feature: Feature to test API

  Scenario: Check API is running and giving desired result
    Given API request page "<PAGE_NUMBER>"
    When API request return a successful response "<STATUS_CODE>"
    Then API request return "<TOTAL_USERS>" users in total
    And API request returns the user with the following information "<FIRST_NAME>""<LAST_NAME>""<EMAIL>""<USER_AVATAR>"
  
  
  Examples:
   |PAGE_NUMBER			|STATUS_CODE		|TOTAL_USERS	|FIRST_NAME			|LAST_NAME	|USER_AVATAR															|EMAIL									|
   |1								|200						|6						|Janet					|Weaver			|https://reqres.in/img/faces/2-image.jpg	|janet.weaver@reqres.in	|
   |2								|200						|6						|Byron					|Fields			|https://reqres.in/img/faces/10-image.jpg	|byron.fields@reqres.in	|
   
   
   Scenario: API having a non-existent
    Given check it "<PAGE_NUMBER2>"
    When API request status "<STATUS_CODE2>"
    Then API having a non-existent "<TOTAL_USERS2>"
   
  Examples:
   |PAGE_NUMBER2			|STATUS_CODE2		|TOTAL_USERS2	|
   |12							|200						|0						|
    